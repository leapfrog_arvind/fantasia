﻿var sql = require("mssql");
var connect = function () {
    var conn = new sql.ConnectionPool({
        server: 'localhost',
        user: 'sa',
        password: 'sa@2014',
        //server: 'localhost',
        database: 'Fantasia',
        options: {
            trustedConnection: true,
            enableArithAbort: true,
            encrypt: false // Use this if you're on Windows Azure
        }
    });

    return conn;
};

module.exports = connect;