var express = require('express');
var router = express.Router();
var sql = require("mssql");
var conn = require("../Connection/Connect")();


//var nStatic = require('node-static');
//var _securityobj = new nStatic.Server('../Utility/Security');



var routes = function () {
    router.route('/')
        .post(function (req, res) {

            //console.log("sdfsdff", req);
            conn.connect().then(function () {
                var transaction = new sql.Transaction(conn);
                transaction.begin().then(function () {
                    var request = new sql.Request(transaction);
                    request.input("user_id", sql.NVarChar(50), req.body.user_id)
                    request.input("post_id", sql.NVarChar(50), req.body.post_id)
                    request.input("Comment", sql.NVarChar(500), req.body.comment)
                    request.input('apiname', sql.NVarChar, 'add_comment');
                    request.execute("comment").then(function (result) {
                        transaction.commit().then(function () {
                            if (result.recordset[0].StatusCode == 200) {

                                res.status(200).json({
                                    status: true,
                                    message: "Add Successfully",
                                    data: null
                                })
                            }
                            else {
                                res.status(200).json({
                                    status: false,
                                    message: "Unauthorised",
                                    data: null
                                })
                            }
                            conn.close();
                        }).catch(function (err) {
                            console.log('err:', err);
                            conn.close();
                            const error = new Error(err.RequestError)
                            error.httpStatusCode = 500
                            return next(error)
                        });

                    }).catch(function (err) {
                        console.log('err:', err);
                        conn.close();
                        const error = new Error(err.RequestError)
                        error.httpStatusCode = 500
                        return next(error)
                    });
                }).catch(function (err) {
                    console.log('err:', err);
                    conn.close();
                    const error = new Error(err.RequestError)
                    error.httpStatusCode = 500
                    return next(error)
                });
            }).catch(function (err) {
                console.log('err:', err);
                conn.close();
                const error = new Error(err.RequestError)
                error.httpStatusCode = 500
                return next(error)
            });
        });

    return router;

};
module.exports = routes;