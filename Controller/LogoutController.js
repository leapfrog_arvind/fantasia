﻿var express = require('express');
var router = express.Router();
var sql = require("mssql");
var conn = require("../Connection/Connect")();


//var nStatic = require('node-static');
//var _securityobj = new nStatic.Server('../Utility/Security');



var routes = function () {
    router.route('/')
        .post(function (req, res) {

            //console.log("sdfsdff", req);
            conn.connect().then(function () {
                var transaction = new sql.Transaction(conn);
                transaction.begin().then(function () {
                    var request = new sql.Request(transaction);
                    //request.input("rAccessToken", sql.NVarChar(50), req.get('AccessToken'))
                    request.input("UserID", sql.NVarChar(500), req.body.UserID)
                    request.execute("User_Logout").then(function (result) {
                        if (result.recordset.length === 0) {
                            res.send(JSON.stringify({
                                "status": 401,
                                "Data": {
                                    "Message": "Data Not Found",
                                },
                            }));
                            conn.close();
                        }
                        else {
                        transaction.commit().then(function () {

                            if (result.recordset[0].ErrorCode == 200) {
                                    res.send(JSON.stringify({
                                        "status": 200,
                                        "Message": "Logout Successfully.",
                                        "Data": {
                                            //"CurrentBooking": result.recordsets[0],
                                        },
                                    }));
                                

                            }
                            else {
                                res.send(JSON.stringify({
                                    "status": 401,
                                    "Data": {
                                        "Message": "Data Not Found",
                                    },
                                }));
                            }
                            conn.close();
                        }).catch(function (err) {
                            conn.close();
                            res.status(400).send("something went to wrong1");
                        });
                        }

                    }).catch(function (err) {
                        conn.close();
                        res.status(400).send("something went to wrong2");
                    });
                }).catch(function (err) {
                    conn.close();
                    res.status(400).send("something went to wrong3");
                });
            }).catch(function (err) {
                conn.close();
                res.status(400).send("something went to wrong4");
            });
        });

    return router;

};
module.exports = routes;