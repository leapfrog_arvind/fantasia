﻿var express = require('express');
var router = express.Router();
var sql = require("mssql");
var conn = require("../Connection/Connect")();


//var nStatic = require('node-static');
//var _securityobj = new nStatic.Server('../Utility/Security');



var routes = function () {
    router.route('/')
        .post(function (req, res) {
                conn.connect().then(function () {
                var transaction = new sql.Transaction(conn);
                transaction.begin().then(function () {
                    var request = new sql.Request(transaction);
                    request.input("userid", sql.NVarChar(250), req.body.userid)
                    request.input("apiname", sql.NVarChar(250), 'notificationlist')
                    request.execute("notification").then(function (result) {
                        //console.log("result", request);
                        if (result.recordset.length === 0) {
                            res.status(200).json({
                                status: false,
                                message: "Data Not Found",
                                data: null
                            })
                            conn.close();
                        }
                        else {
                            transaction.commit().then(function () {

                            if (result.recordset[0].StatusCode == 200) {
                                res.status(200).json({
                                    status: true,
                                    message: "Success",
                                    data: result.recordsets[0]
                                })
                            }
                            else {
                                res.status(200).json({
                                    status: false,
                                    message: "Data Not Found",
                                    data: null
                                })
                            }
                            conn.close();
                        }).catch(function (err) {
                            conn.close();
                            res.status(400).send("something went to wrong1");
                        });
                        }

                    }).catch(function (err) {
                        conn.close();
                        res.status(400).send(err);
                    });
                }).catch(function (err) {
                    conn.close();
                    res.status(400).send("something went to wrong3");
                });
            }).catch(function (err) {
                conn.close();
                res.status(400).send("something went to wrong4");
            });
        });

    return router;

};
module.exports = routes;