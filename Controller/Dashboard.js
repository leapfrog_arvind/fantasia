﻿var express = require('express');
var router = express.Router();
var sql = require("mssql");
var conn = require("../Connection/Connect")();


//var nStatic = require('node-static');
//var _securityobj = new nStatic.Server('../Utility/Security');



var routes = function () {
    router.route('/')
        .post(function (req, res) {
                conn.connect().then(function () {
                var transaction = new sql.Transaction(conn);
                transaction.begin().then(function () {
                    var request = new sql.Request(transaction);
                    request.input("user_id", sql.NVarChar(250), req.body.user_id)
                    request.execute("DashboardList").then(function (result) {
                        // console.log("result", result.recordset);
                        if (result.recordset.length === 0) {
                            res.status(200).json({
                                status: false,
                                message: "Data Not Found",
                                data: null
                            })
                            conn.close();
                        }
                        else {
                            transaction.commit().then(function () {

                            if (result.recordset[0].StatusCode == 200) {
                                // res.status(200).json({
                                //     status: true,
                                //     message: "Success",
                                //     data: result.recordsets[0]
                                // })
                                res.send(JSON.stringify({
                                    "status": 200,
                                    "Message": "Success message.",
                                    "Data": {
                                        "user_story": result.recordsets[0],
                                        "user_post": result.recordsets[1],
                                       
                                    },
                                }));
                            }
                            else {
                                res.status(200).json({
                                    status: false,
                                    message: "Data Not Found",
                                    data: null
                                })
                            }
                            conn.close();
                        }).catch(function (err) {
                            conn.close();
                            res.status(400).send("something went to wrong1");
                        });
                        }

                    }).catch(function (err) {
                        conn.close();
                        res.status(400).send(err);
                    });
                }).catch(function (err) {
                    conn.close();
                    res.status(400).send("something went to wrong3");
                });
            }).catch(function (err) {
                conn.close();
                res.status(400).send("something went to wrong4");
            });
        });

    return router;

};
module.exports = routes;