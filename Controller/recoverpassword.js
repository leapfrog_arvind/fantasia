var express = require('express');
var router = express.Router();
var sql = require("mssql");
var conn = require("../Connection/Connect")();


//var nStatic = require('node-static');
//var _securityobj = new nStatic.Server('../Utility/Security');



var routes = function () {
    router.route('/')
        .post(function (req, res) {
            conn.connect().then(function () {
                var transaction = new sql.Transaction(conn);
                transaction.begin().then(function () {
                    var request = new sql.Request(transaction);
                    request.input("user_id", sql.NVarChar(20), req.body.user_id)
                    request.input("OTPCode", sql.NVarChar(20), req.body.reset_code)
                    request.input("new_password", sql.NVarChar(50), req.body.new_password)
                    request.input("confirm_password", sql.NVarChar(50), req.body.confirm_password)
                    request.input('apiname', sql.NVarChar, 'ChangePassword');
                    request.execute("Register").then(function (result) {
                        transaction.commit().then(function () {
                            console.log('responce:',result.recordset[0]);
                            
                            if (result.recordset[0].ErrorCode == 200) {
                                //Console.log("respo:=", recordset);
                                res.status(200).json({
                                    status: true,
                                    message: "Your Password Change successfully",
                                    data: result.recordset[0]
                                })
                            }

                            else {
                                res.status(200).json({
                                    status: false,
                                    message: "Reset code does not exist",
                                    data: null
                                })
                            }
                            conn.close();
                        }).catch(function (err) {
                            conn.close();
                            const error = new Error(err)
                            error.httpStatusCode = 500
                            return next(error)
                        });
                    }).catch(function (err) {
                        conn.close();
                        const error = new Error(err)
                        error.httpStatusCode = 500
                        return next(error)
                    });
                }).catch(function (err) {
                    conn.close();
                    const error = new Error(err)
                    error.httpStatusCode = 500
                    return next(error)
                });
            }).catch(function (err) {
                conn.close();
                const error = new Error(err)
                error.httpStatusCode = 500
                return next(error)
            });
        });

    return router;

};
module.exports = routes;