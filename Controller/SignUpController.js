﻿var express = require('express');
var router = express.Router();
var sql = require("mssql");
var conn = require("../Connection/Connect")();
const multer = require('multer');
var uniqid = require('uniqid');
var nodemailer = require('nodemailer');

// const storage = multer.diskStorage({
// destination: function (req, file, cb) {
// cb(null, './Content/UsersImage/');
// },
// filename: function (req, file, cb) {
// cb(null, uniqid() + file.originalname);
// }
// });

// const upload = multer({ storage: storage })

var routes = function () {
    
    
    router.route('/')
        .post(function (req, res, next) {

            conn.connect().then(function () {
                var transaction = new sql.Transaction(conn);
                transaction.begin().then(function () {
                    // console.log('Dipak');
                    // console.log('transaction.begin()',transaction.begin());
                    
                    var request = new sql.Request(transaction);
                    request.input("first_name", sql.NVarChar(50), req.body.first_name)
                    request.input("last_name", sql.NVarChar(50), req.body.last_name)
                    request.input("email_id", sql.NVarChar(75), req.body.email_id)
                    request.input("mobile_no", sql.NVarChar(20), req.body.mobile_no)
                    request.input("Password", sql.NVarChar(250), req.body.Password)
                    request.input("address", sql.NVarChar(500), req.body.address)
                    request.input("device_type", sql.NVarChar(15), req.body.device_type)
                    request.input("device_id", sql.NVarChar(250), req.body.device_id)
                    request.input('apiname', sql.NVarChar, 'Register');
                    if (!req.file) {
						// const imagepath = req.file ? '/Images/' + req.file.filename : null
                       
                    }
					else{
						 request.input("rUserImagePath", sql.NVarChar(500), "Image/" + req.file.filename)
					}
                    request.execute("Register").then(function (result) {
                        console.log('request:',request);
                        
                        transaction.commit().then(function () {
                            
                            
                            if (result.recordset[0].ErrorCode == 200) {
                                //Console.log("respo:=", recordset);
                                res.status(200).json({
                                    status: true,
                                    message: "Registration Successfully",
                                    data: result.recordset[0]
                                })
                                
                            }

                            else {
                                res.status(200).json({
                                    status: false,
                                    message: "This Email or Mobile number already exists.",
                                    data: null
                                })
                            }
                            conn.close();

                        }).catch(function (err) {
                            console.log('err:',err);
                            
                            conn.close();
                            const error = new Error(err)
                            error.httpStatusCode = 500
                            return next(error)
                            // res.status(400).send(err);
                        });
                    }).catch(function (err) {
                        console.log('err:',err);
                            
                        conn.close();
                        const error = new Error(err)
                        error.httpStatusCode = 501
                        return next(error)
                    });
                }).catch(function (err) {
                    console.log('err:',err);
                    
                    conn.close();
                    const error = new Error(err)
                    error.httpStatusCode = 502
                    res.send(error);
                    // return next(error)
                });
            }).catch(function (err) {
                conn.close();
                const error = new Error(err)
                error.httpStatusCode = 503
                return next(error)
            });
        });

    return router;

};
module.exports = routes;









