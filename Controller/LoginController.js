﻿var express = require('express');
var router = express.Router();
var sql = require("mssql");
var conn = require("../Connection/Connect")();


//var nStatic = require('node-static');
//var _securityobj = new nStatic.Server('../Utility/Security');



var routes = function () {
    router.route('/')
        .post(function (req, res) {

            //console.log("sdfsdff", req);
            conn.connect().then(function () {
                var transaction = new sql.Transaction(conn);
                transaction.begin().then(function () {
                    var request = new sql.Request(transaction);
                    request.input("email_id", sql.NVarChar(250), req.body.email_id)
                    request.input("Password", sql.NVarChar(250), req.body.Password)
                    request.input("device_id", sql.NVarChar(500), req.body.device_id)
                    request.input("device_type", sql.NVarChar(50), req.body.device_type)
                    request.input('apiname', sql.NVarChar, 'Login');

                    request.execute("Register").then(function (result) {
                        transaction.commit().then(function () {
                            console.log('result.recordset[0]:',result.recordset[0]);
                            
                            if (result.recordset[0].ErrorCode == 200) {
                                res.status(200).json({
                                    status: true,
                                    message: "You have logged in successfully.",
                                    data: result.recordset[0]
                                })
                            }
                            else {
                                res.status(200).json({
                                    status: false,
                                    message: "Invalid email id and password!",
                                    data: null
                                })

                            }
                            conn.close();
                        }).catch(function (err) {
                            console.log('err:', err);

                            conn.close();
                            const error = new Error(err.RequestError)
                            error.httpStatusCode = 500
                            return next(error)
                        });
                    }).catch(function (err) {
                        console.log('err:', err);

                        conn.close();
                        const error = new Error(err.RequestError)
                        error.httpStatusCode = 500
                        return next(error)
                    });
                }).catch(function (err) {
                    console.log('err:', err);

                    conn.close();
                    const error = new Error(err.RequestError)
                    error.httpStatusCode = 500
                    return next(error)
                });
            }).catch(function (err) {
                console.log('err:', err);

                conn.close();
                const error = new Error(err.RequestError)
                error.httpStatusCode = 500
                return next(error)
            });
        });

    return router;

};
module.exports = routes;