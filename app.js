var createError = require('http-errors');
var express = require('express');
var path = require('path');
const multer = require('multer');


var app = express();

// view engine setup

// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');


app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));


const fileStorage = multer.diskStorage({
  // console.log('Dipak');
  
  destination: (req, file, cb) => {
    cb(null, path.join(__dirname,'/Content/UsersImage/'));
	//cb(null, '../Images');
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  }
});

app.use(multer({ storage: fileStorage }).single('filename'));

var Login = require('./Controller/LoginController')();
app.use("*/api/login", Login);

var Forgot = require('./Controller/Forgot')();
app.use("*/api/forgot_password", Forgot);

var changepassword = require('./Controller/changepassword')();
app.use("*/api/change_password", changepassword);

var SignUpController = require('./Controller/SignUpController')();
app.use("*/api/sign_up", SignUpController);

var recoverpassword = require('./Controller/recoverpassword')();
app.use("*/api/recover_password", recoverpassword);

var Dashboard = require('./Controller/Dashboard')();
app.use("*/api/dashboard", Dashboard);

var AddComment = require('./Controller/AddComment')();
app.use("*/api/add_comment", AddComment);

var NotificationController = require('./Controller/NotificationController')();
app.use("*/api/notification", NotificationController);

var AddFavourite = require('./Controller/AddFavourite')();
app.use("*/api/like_unlike", AddFavourite);

var FavouriteList = require('./Controller/FavouriteList')();
app.use("*/api/like_list", FavouriteList);

var commentlist = require('./Controller/commentlist')();
app.use("*/api/comment_list", commentlist);


var LogoutController = require('./Controller/LogoutController')();
app.use("*/api/Logout", LogoutController);



app.get('*/Fantasia/api/Image/:imagename', function (req, res) {
    console.log('dipak');
    var options = {
        root: path.join(__dirname, './Content/UsersImage'),
        dotfiles: 'deny',
        headers: {
            'x-timestamp': new Date().toJSON().replace(new RegExp(':', 'g'), '.'),
            'x-sent': true
        }
    };
    res.sendFile(req.params.imagename, options)
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send(err);
});

module.exports = app;
